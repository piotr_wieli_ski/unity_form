﻿using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using UnityForm.Web.Contracts;
using UnityForm.Web.Models;
using UnityForm.Web.Services;

namespace UnityForm.Web.Controllers
{
    public class FormController : BaseController
    {
        private readonly ICustomerService _service;

        public FormController()
        {
            _service = new CustomerService(Context);
        }

        private List<City> GetCities()
        {
            if (TempData.ContainsKey("Cities"))
                return (List<City>) TempData.Peek("Cities");

            var cities = Context.City.ToList();
            TempData.Add("Cities", cities);

            return cities;
        }

        [HttpGet]
        public ActionResult Index()
        {
            var model = new FormViewModel();

            var cities = GetCities();
            model.Address.Cities = cities;
            model.CorrespondenceAddress.Cities = cities;

            return View(model);
        }

        [HttpPost]
        public ActionResult Add(FormViewModel model)
        {
            if (ModelState.IsValid)
            {
                var addressId = _service.AddCustomerAddress(model.Address);
                var correspondaceAddressId = model.IsCorrespondenceAddressSame
                    ? addressId
                    : _service.AddCustomerAddress(model.CorrespondenceAddress);

                _service.AddCustomer(model.Customer, addressId, correspondaceAddressId);

                return RedirectToAction("Index", "Home", new {messageId = 1});
            }

            var cities = GetCities();
            model.Address.Cities = cities;
            model.CorrespondenceAddress.Cities = cities;
            return View("Index", model);
        }
    }
}