﻿using System.Web.Mvc;
using FormContext = UnityForm.Web.DataAccess.FormContext;

namespace UnityForm.Web.Controllers
{
    public class BaseController : Controller
    {
        protected FormContext Context;

        public BaseController()
        {
            Context = new FormContext();
            Context.Database.CreateIfNotExists();
        }
    }
}