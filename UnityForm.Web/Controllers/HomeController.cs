﻿using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using UnityForm.Web.Models;

namespace UnityForm.Web.Controllers
{
    public class HomeController : BaseController
    {
        [HttpGet]
        public ActionResult Index(int messageId = 0)
        {
            if (messageId == 1)
                ViewBag.Message = "Formularz został poprawnie zapisany.";

            var data = Context.Customer.Any() ? Context.Customer.ToList() : new List<Customer>();

            return View(data);
        }
    }
}