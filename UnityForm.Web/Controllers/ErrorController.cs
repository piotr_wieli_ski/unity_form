﻿using System.Net;
using System.Web.Mvc;

namespace UnityForm.Web.Controllers
{
    public class ErrorController : Controller
    {
        public ViewResult NotFound()
        {
            Response.TrySkipIisCustomErrors = true;
            Response.StatusCode = (int)HttpStatusCode.NotFound;
            return View("NotFound");
        }

        public ViewResult Unknown()
        {
            Response.StatusCode = (int)HttpStatusCode.OK;
            Response.TrySkipIisCustomErrors = true;
            return View("Unknown");
        }
    }
}