﻿using System.Collections.Generic;
using UnityForm.Web.Models;

namespace UnityForm.Web.DataAccess
{
    public class CitiesInitializer : System.Data.Entity.DropCreateDatabaseIfModelChanges<FormContext>
    {
        protected override void Seed(FormContext context)
        {
            var cites = new List<City>
            {
                new City {Province = "dolnośląskie", County = "Wrocław", Name = "Wrocław"},
                new City {Province = "dolnośląskie", County = "wrocławski", Name = "Długołęka"},
                new City {Province = "dolnośląskie", County = "wrocławski", Name = "Kąty Wrocławskie"},
                new City {Province = "dolnośląskie", County = "wrocławski", Name = "Kobierzyce"},
                new City {Province = "dolnośląskie", County = "wrocławski", Name = "Siechnice"},
                new City {Province = "dolnośląskie", County = "wrocławski", Name = "Sobótka"},
                new City {Province = "dolnośląskie", County = "wrocławski", Name = "Czernica"},
                new City {Province = "dolnośląskie", County = "wrocławski", Name = "Żórawina"},
                new City {Province = "dolnośląskie", County = "wrocławski", Name = "Mietków"},
                new City {Province = "dolnośląskie", County = "wrocławski", Name = "Jordanów Śląski"},
                new City {Province = "dolnośląskie", County = "Legnica", Name = "Legnica"},
                new City {Province = "dolnośląskie", County = "legnicki", Name = "Chojnów"},
                new City {Province = "dolnośląskie", County = "legnicki", Name = "Prochowice"},
                new City {Province = "dolnośląskie", County = "legnicki", Name = "Miłkowice"},
                new City {Province = "dolnośląskie", County = "legnicki", Name = "Kunice"},
                new City {Province = "dolnośląskie", County = "legnicki", Name = "Legnickie Pole"},
                new City {Province = "dolnośląskie", County = "legnicki", Name = "Krotoszyce"},
                new City {Province = "dolnośląskie", County = "legnicki", Name = "Ruja"},
                new City {Province = "wielkopolskie", County = "Poznań", Name = "Poznań"},
                new City {Province = "wielkopolskie", County = "poznański", Name = "Luboń"},
                new City {Province = "wielkopolskie", County = "poznański", Name = "Puszczykowo"},
                new City {Province = "wielkopolskie", County = "poznański", Name = "Buk"},
                new City {Province = "wielkopolskie", County = "poznański", Name = "Kostrzyn"},
                new City {Province = "wielkopolskie", County = "poznański", Name = "Mosina"},
                new City {Province = "wielkopolskie", County = "poznański", Name = "Murowana Goślina"},
                new City {Province = "wielkopolskie", County = "poznański", Name = "Pobiedziska"},
                new City {Province = "wielkopolskie", County = "poznański", Name = "Stęszew"},
                new City {Province = "wielkopolskie", County = "poznański", Name = "Swarzędz"},
                new City {Province = "wielkopolskie", County = "poznański", Name = "Czerwonak"},
                new City {Province = "wielkopolskie", County = "poznański", Name = "Dopiewo"},
                new City {Province = "wielkopolskie", County = "poznański", Name = "Kleszczewo"},
                new City {Province = "wielkopolskie", County = "poznański", Name = "Komorniki"},
                new City {Province = "wielkopolskie", County = "poznański", Name = "Rokietnica"},
                new City {Province = "wielkopolskie", County = "poznański", Name = "Suchy Las"},
                new City {Province = "wielkopolskie", County = "poznański", Name = "Tarnowo Podgórne"},
                new City {Province = "opolskie", County = "Opole", Name = "Opole"},
                new City {Province = "śląskie", County = "gliwicki", Name = "Knurów"},
                new City {Province = "śląskie", County = "gliwicki", Name = "Pyskowice"},
                new City {Province = "śląskie", County = "gliwicki", Name = "Sośnicowice"},
                new City {Province = "śląskie", County = "gliwicki", Name = "Toszek"},
            };

            cites.ForEach(c => context.City.Add(c));
            context.SaveChanges();
        }
    }
}