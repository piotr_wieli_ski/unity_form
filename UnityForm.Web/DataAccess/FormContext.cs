﻿using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Data.Entity.ModelConfiguration.Conventions;
using UnityForm.Web.Models;

namespace UnityForm.Web.DataAccess
{
    public class FormContext : DbContext
    {
        public FormContext() : base("UnityDB")
        {
        }

        public DbSet<Customer> Customer { get; set; }
        public DbSet<Address> Address { get; set; }
        public DbSet<City> City { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Customer>().Property(p => p.FirstName).HasMaxLength(100).IsRequired().IsUnicode();
            modelBuilder.Entity<Customer>().Property(p => p.LastName).HasMaxLength(100).IsRequired().IsUnicode();
            modelBuilder.Entity<Customer>().Property(p => p.DateOfBirth).IsRequired();
            modelBuilder.Entity<Customer>().Property(p => p.AddressId).IsRequired();
            modelBuilder.Entity<Customer>().HasRequired(c => c.Address)
                .WithMany()
                .WillCascadeOnDelete(false);
            modelBuilder.Entity<Customer>().Property(p => p.CorrespondenceAddressId).IsRequired();
            modelBuilder.Entity<Customer>().HasRequired(c => c.CorrespondenceAddress)
                .WithMany()
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Address>().Property(p => p.Street).HasMaxLength(255).IsRequired().IsUnicode();
            modelBuilder.Entity<Address>().Property(p => p.StreetNo).HasMaxLength(10).IsRequired().IsUnicode();
            modelBuilder.Entity<Address>().Property(p => p.PostCode).IsRequired();
            modelBuilder.Entity<Address>().Property(p => p.CityId).IsRequired();

            modelBuilder.Entity<City>().Property(p => p.Name).HasMaxLength(100).IsRequired().IsUnicode();
            modelBuilder.Entity<City>().Property(p => p.Province).HasMaxLength(50).IsRequired().IsUnicode();
            modelBuilder.Entity<City>().Property(p => p.County).HasMaxLength(50).IsRequired().IsUnicode();

            modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();
        }
    }
}