﻿using System;
using System.Web;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;

namespace UnityForm.Web
{
    public class MvcApplication : HttpApplication
    {
        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);
        }

        protected void Application_Error(object sender, EventArgs e)
        {
            var unhandledException = Server.GetLastError() as HttpException;

            Response.Clear();
            Server.ClearError();

            if (unhandledException != null)
            {
                var errorCode = unhandledException.GetHttpCode();

                if (errorCode == 404)
                {
                    Response.Redirect("~/Error/NotFound");
                    return;
                }
            }
            Response.Redirect("~/Error/Unknown");
        }
    }
}