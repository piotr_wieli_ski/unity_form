﻿namespace UnityForm.Web.Models
{
    public class Address : Entity
    {
        public string Street { get; set; }
        public string StreetNo { get; set; }
        public string PostCode { get; set; }

        public int CityId { get; set; }
        public virtual City City { get; set; }
    }
}