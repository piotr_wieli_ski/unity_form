namespace UnityForm.Web.Models
{
    public class City : Entity
    {
        public string Name { get; set; }
        public string County { get; set; }
        public string Province { get; set; }
    }
}