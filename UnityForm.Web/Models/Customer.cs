﻿using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace UnityForm.Web.Models
{
    public class Customer : Entity
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public DateTime DateOfBirth { get; set; }

        public int AddressId { get; set; }

        [ForeignKey("AddressId")]
        public virtual Address Address { get; set; }

        public int CorrespondenceAddressId { get; set; }

        [ForeignKey("CorrespondenceAddressId")]
        public virtual Address CorrespondenceAddress { get; set; }
    }
}