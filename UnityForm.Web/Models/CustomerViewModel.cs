﻿using System;
using System.ComponentModel.DataAnnotations;

namespace UnityForm.Web.Models
{
    public class CustomerViewModel
    {
        [Required]
        [Display(Name = "Imię")]
        public string FirstName { get; set; }

        [Required]
        [Display(Name = "Nazwisko")]
        public string LastName { get; set; }

        [Required]
        [Display(Name = "Data urodzenia")]
        [DataType(DataType.Date, ErrorMessage = "Podana wartość jest nieprawidłowa dla daty urodzenia.")]
        public DateTime? DateOfBirth { get; set; }
    }
}