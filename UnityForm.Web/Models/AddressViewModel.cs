﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace UnityForm.Web.Models
{
    public class AddressViewModel
    {
        public IEnumerable<City> Cities { get; set; }

        [Display(Name = "Ulica")]
        public string Street { get; set; }

        [Display(Name = "Numer")]
        public string StreetNo { get; set; }

        [Display(Name = "Kod Pocztowy")]
        public string PostCode { get; set; }

        [Display(Name = "Miasto")]
        public int? CityId { get; set; }
    }
}