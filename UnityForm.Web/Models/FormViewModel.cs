﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace UnityForm.Web.Models
{
    public class FormViewModel : IValidatableObject
    {
        public FormViewModel()
        {
            Address = new AddressViewModel();
            CorrespondenceAddress = new AddressViewModel();
        }

        public CustomerViewModel Customer { get; set; }

        public AddressViewModel Address { get; set; }

        [Display(Name = "Adres korespondencyjny taki jak adres zameldowania")]
        public bool IsCorrespondenceAddressSame { get; set; }

        public AddressViewModel CorrespondenceAddress { get; set; }

        public IEnumerable<ValidationResult> Validate(ValidationContext validationContext)
        {
            if (string.IsNullOrWhiteSpace(Address.Street))
                yield return new ValidationResult("Pole Ulica jest wymagane.", new[] {"Address.Street"});
            if (string.IsNullOrWhiteSpace(Address.StreetNo))
                yield return new ValidationResult("Pole Numer jest wymagane.", new[] {"Address.StreetNo"});
            if (Address.CityId == null)
                yield return new ValidationResult("Pole Miasto jest wymagane.", new[] {"Address.CityId"});
            if (string.IsNullOrWhiteSpace(Address.PostCode))
                yield return new ValidationResult("Pole Kod pocztowy jest wymagane.", new[] {"Address.PostCode"});

            if (!IsCorrespondenceAddressSame)
            {
                if (string.IsNullOrWhiteSpace(CorrespondenceAddress.Street))
                    yield return
                        new ValidationResult("Pole Ulica jest wymagane.", new[] {"CorrespondenceAddress.Street"});
                if (string.IsNullOrWhiteSpace(CorrespondenceAddress.StreetNo))
                    yield return
                        new ValidationResult("Pole Numer jest wymagane.", new[] {"CorrespondenceAddress.StreetNo"});
                if (CorrespondenceAddress.CityId == null)
                    yield return
                        new ValidationResult("Pole Miasto jest wymagane.", new[] {"CorrespondenceAddress.CityId"});
                if (string.IsNullOrWhiteSpace(CorrespondenceAddress.PostCode))
                    yield return
                        new ValidationResult("Pole Kod pocztowy jest wymagane.",
                            new[] {"CorrespondenceAddress.PostCode"});
            }
        }
    }
}