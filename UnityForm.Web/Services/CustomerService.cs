﻿using UnityForm.Web.Contracts;
using UnityForm.Web.DataAccess;
using UnityForm.Web.Models;

namespace UnityForm.Web.Services
{
    public class CustomerService : ICustomerService
    {
        private readonly FormContext _context;

        public CustomerService(FormContext context)
        {
            _context = context;
        }

        public int AddCustomerAddress(AddressViewModel model)
        {
            var address = new Address
            {
                CityId = model.CityId.Value,
                PostCode = model.PostCode,
                StreetNo = model.StreetNo,
                Street = model.Street
            };
            _context.Address.Add(address);
            _context.SaveChanges();

            return address.Id;
        }

        public void AddCustomer(CustomerViewModel model, int addressId, int correspondenceAddressId)
        {
            var customer = new Customer
            {
                FirstName = model.FirstName,
                LastName = model.LastName,
                DateOfBirth = model.DateOfBirth.Value,
                AddressId = addressId,
                CorrespondenceAddressId = correspondenceAddressId
            };

            _context.Customer.Add(customer);
            _context.SaveChanges();
        }
    }
}