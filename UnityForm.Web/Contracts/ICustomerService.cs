﻿using UnityForm.Web.Models;

namespace UnityForm.Web.Contracts
{
    public interface ICustomerService
    {
        int AddCustomerAddress(AddressViewModel model);

        void AddCustomer(CustomerViewModel model, int addressId, int correspondenceAddressId);
    }
}